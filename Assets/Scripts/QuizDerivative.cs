﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuizDerivative : MonoBehaviour
{

    //Q1 items
    public Image q3questionImage;
    public Canvas q3_derivative_final;
    public Canvas q3_derivative_start;
    public Canvas q3_questionCanvas;
    public Canvas q3_raspuns_corect;
    public Canvas q3_raspuns_gresit;
    public Button q3a1;
    public Button q3a2;
    public Button q3a3;
    public Button q3a4;

    public List<Quiz> listaIntrebari;
    public Quiz quizDerivative;
    private String intrebare;
    private List<Item> listaQuiz;
    private static int quizNumber = 0;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        GetQuizArray();

        q3questionImage = q3questionImage.GetComponent<Image>();
        q3questionImage.enabled = false;

        q3_questionCanvas = q3_questionCanvas.GetComponent<Canvas>();
        q3_questionCanvas.enabled = false;

        q3_derivative_start = q3_derivative_start.GetComponent<Canvas>();
        q3_derivative_start.enabled = true;

        q3_raspuns_corect = q3_raspuns_corect.GetComponent<Canvas>();
        q3_raspuns_corect.enabled = false;

        q3_raspuns_gresit = q3_raspuns_gresit.GetComponent<Canvas>();
        q3_raspuns_gresit.enabled = false;

        q3_derivative_final = q3_derivative_final.GetComponent<Canvas>();
        q3_derivative_final.enabled = false;

        q3_questionCanvas = q3_questionCanvas.GetComponent<Canvas>();

        q3a1 = q3a1.GetComponent<Button>();
        q3a1.enabled = false;

        q3a2 = q3a2.GetComponent<Button>();
        q3a2.enabled = false;

        q3a3 = q3a3.GetComponent<Button>();
        q3a3.enabled = false;

        q3a4 = q3a4.GetComponent<Button>();
        q3a4.enabled = false;
    }

    private void GetQuizArray()
    {
        listaIntrebari = new List<Quiz>();
        quizDerivative = new Quiz();
        listaQuiz = new List<Item>();
        //Item1
        intrebare = "Un instrument financiar derivat reprezinta";
        Item i1 = new Item(false, "un bon de tezuar emis de trezorerie");
        Item i3 = new Item(false, "o actiune cedata prin mostenire");
        Item i2 = new Item(true, "un instrument al carui pret depinde de valoarea unui alt activ");
        Item i4 = new Item(false, "niciuna din variantele de mai sus");
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare);

        listaIntrebari.Add(quizDerivative);

        //Item2
        string intrebare2 = "Un contract de tip swap";
        i2 = new Item(false, "defineste un contract vandut de catre o partida unei contrapartide");
        i1 = new Item(true, "permite ca doua entitati sa schimbe intre ele instrumente financiare");
        i3 = new Item(false, "permite detinatorului sa cumpere/vanda un activ la un pret si o data viitoare");
        i4 = new Item(false, "ofera dreptul, dar nu si obligatia de a vinde moneda la o anumita rata de schimb");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare2);

        listaIntrebari.Add(quizDerivative);

        //Item3
        string intrebare3 = "Credit Default Swaps (CDS) sunt instrumente derivate ce";
        i1 = new Item(false, "prezinta un risc ridicat si trebuie tranzactionate doar de catre persoanele instarite");
        i2 = new Item(false, "nu obliga cumparatorul/vanzatorul sa gireze printr-un bun colateral");
        i3 = new Item(false, "ar trebui utilizate de persoane ce urmaresc profituri mari asumandu-si riscuri limitate");
        i4 = new Item(true, "permit creditorilor sa se asigure impotriva riscului de faliment al debitorilor");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare3);

        listaIntrebari.Add(quizDerivative);

        //Item4
        string intrebare4 = "Scopul instrumentelor derivate este sa";
        i1 = new Item(false, "mareasca profitabilitatea unui activ suport");
        i2 = new Item(true, "transfere riscul de la o entitate la alta");
        i3 = new Item(false, "incurajeze speculatia");
        i4 = new Item(false, "ascunda adevarata natura a unor tranzactii financiare");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare4);

        listaIntrebari.Add(quizDerivative);

        //Item5
        string intrebare5 = "Un contract forward";
        i2 = new Item(false, "este un contract standardizat si vandut prin organizatii specializate");
        i1 = new Item(true, "este un acord intre parti de a tranzactiona active la un pret si o data stabilite");
        i3 = new Item(false, "nu implica existenta unui contract");
        i4 = new Item(false, "nu este un instrument derivat");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare5);

        listaIntrebari.Add(quizDerivative);

        //Item6
        string intrebare6 = "Care dintre urmatoarele este o diferenta intre contractele de tip SWAP si Future";
        i1 = new Item(false, "future implica o singura tranzactie viitoare, swap mai multe");
        i4 = new Item(false, "swap sunt pe termen scurt, future pe termen lung");
        i3 = new Item(true, "swap sunt evaluate la pretul pietei, futures nu");
        i2 = new Item(false, "swap este un instrument derivat, futures nu");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare6);

        listaIntrebari.Add(quizDerivative);

        //Item7
        string intrebare7 = "Utilizatea contractelor futures penru a transfera riscul se numeste:";
        i1 = new Item(false, "specula");
        i2 = new Item(false, "diversiune");
        i3 = new Item(false, "acoperire");
        i4 = new Item(true, "arbitraj");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare7);

        listaIntrebari.Add(quizDerivative);

        //Item8
        string intrebare8 = "Contractele futures standardizate nu pot avea ca activ suport:";
        i1 = new Item(false, "indecsi bancari");
        i2 = new Item(true, "actiuni intr-o firma");
        i3 = new Item(false, "titluri de stat");
        i4 = new Item(false, "aur");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizDerivative = new Quiz();
        quizDerivative.setLista(listaQuiz);
        quizDerivative.setIntrebare(intrebare8);

        listaIntrebari.Add(quizDerivative);

    }

    public void EvaluateAnswer()
    {
        GameObject selectedElement = EventSystem.current.currentSelectedGameObject;
        Text textContainer = selectedElement.GetComponentInChildren<Text>();
        string ut = textContainer.text;
        Quiz q = listaIntrebari[quizNumber];
        List<Item> li = q.getLista();
        int i;
        for (i = 0; i < 4; i++)
        {
            if (ut.Equals(li[i].getValue()))
                break;
        }
        if (true == li[i].getType())
            CorrectButtonPress();
        else
            OtherButtonPress();
    }

    public void CorrectButtonPress()
    {
        quizNumber++;
        q3_raspuns_corect.enabled = true;
        q3_questionCanvas.enabled = false;
        q3_derivative_start.enabled = false;
        q3_raspuns_gresit.enabled = false;
        q3_derivative_final.enabled = false;
        q3a1.enabled = false;
        q3a2.enabled = false;
        q3a3.enabled = false;
        q3a4.enabled = false;
    }

    public void OtherButtonPress()
    {
        q3_derivative_final.enabled = false;
        q3_raspuns_gresit.enabled = true;
        q3_questionCanvas.enabled = false;
        q3_raspuns_corect.enabled = false;
        q3_derivative_start.enabled = false;
        q3a1.enabled = false;
        q3a2.enabled = false;
        q3a3.enabled = false;
        q3a4.enabled = false;
    }

    public void Replay()
    {
        q3_derivative_final.enabled = false;
        q3_raspuns_gresit.enabled = false;
        q3_derivative_start.enabled = false;
        q3_questionCanvas.enabled = true;
        q3_raspuns_corect.enabled = false;
        q3a1.enabled = true;
        q3a2.enabled = true;
        q3a3.enabled = true;
        q3a4.enabled = true;
    }

    public void Next()
    {
        if (quizNumber == 8)
        {
            quizNumber++;

            q3_derivative_final.enabled = true;
            q3_raspuns_gresit.enabled = false;
            q3_questionCanvas.enabled = false;
            q3_derivative_start.enabled = false;
            q3_raspuns_corect.enabled = false;
            q3a1.enabled = false;
            q3a2.enabled = false;
            q3a3.enabled = false;
            q3a4.enabled = false;

            return;
        }

        if (quizNumber == 9)
        {
            q3questionImage.enabled = false;
            q3_derivative_final.enabled = false;
            q3_raspuns_gresit.enabled = false;
            q3_derivative_start.enabled = false;
            q3_questionCanvas.enabled = false;
            q3_raspuns_corect.enabled = false;
            q3a1.enabled = false;
            q3a2.enabled = false;
            q3a3.enabled = false;
            q3a4.enabled = false;
            SharesFading.sharesLevelNotFinished = false;
            StartCoroutine(WaitMap());
            return;
        }
        q3_derivative_start = q3_derivative_start.GetComponent<Canvas>();
        q3_derivative_start.enabled = false;

        q3_raspuns_corect = q3_raspuns_corect.GetComponent<Canvas>();
        q3_raspuns_corect.enabled = false;

        q3_raspuns_gresit = q3_raspuns_gresit.GetComponent<Canvas>();
        q3_raspuns_gresit.enabled = false;

        q3_questionCanvas = q3_questionCanvas.GetComponent<Canvas>();
        q3_questionCanvas.enabled = true;

        Text questionText = GameObject.Find("Q3Question").GetComponent<Text>();
        questionText.text = listaIntrebari[quizNumber].getIntrebare();

        q3a1 = q3a1.GetComponent<Button>();
        q3a2 = q3a2.GetComponent<Button>();
        q3a3 = q3a3.GetComponent<Button>();
        q3a4 = q3a4.GetComponent<Button>();

        List<Item> Q1answers = listaIntrebari[quizNumber].getLista();
        Text q3a1text = GameObject.Find("Q3A1Text").GetComponent<Text>();
        q3a1text.text = Q1answers[0].getValue();
        Text q3a2text = GameObject.Find("Q3A2Text").GetComponent<Text>();
        q3a2text.text = Q1answers[1].getValue();
        Text q3a3text = GameObject.Find("Q3A3Text").GetComponent<Text>();
        q3a3text.text = Q1answers[2].getValue();
        Text q3a4text = GameObject.Find("Q3A4Text").GetComponent<Text>();
        q3a4text.text = Q1answers[3].getValue();

        q3a1.enabled = true;
        q3a2.enabled = true;
        q3a3.enabled = true;
        q3a4.enabled = true;
    }

    IEnumerator WaitMap()
    {
        yield return new WaitForSeconds(1.5f);
        Application.LoadLevel("Map");
    }
}
