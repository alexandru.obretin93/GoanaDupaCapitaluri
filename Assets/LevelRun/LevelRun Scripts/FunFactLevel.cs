﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum FunFactLevel {
    Level1 = 1,
    Level2 = 2,
    Level3 = 3
}
