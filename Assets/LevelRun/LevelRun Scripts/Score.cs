﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	private float score = 0.0f;
	private int levelDifficulty = 1;
	private int scoreToNextLevel;
	public int scoreToUpgrade;
	public int distanceToFinish;
	public Text scoreText;
	private bool isDead = false;
	private bool isFunFactDisplay = false;
	public RestartMenu restartMenu; 
	private FunFactManager funFactManager;
	
	void Start(){
		scoreToNextLevel = scoreToUpgrade;
		funFactManager = GameObject.FindGameObjectWithTag(Constants.Tags.FunFactInfo).GetComponent<FunFactManager>();
	} 

	// Update is called once per frame
	void Update () {
        distanceToFinish = scoreToUpgrade * (Constants.FunFacts[FunFactManager.level].Length);
        if (isDead || isFunFactDisplay)
			return;
		if(score >= distanceToFinish)
		{
			StartCoroutine(LevelFinished());
			return;
		}
		if(score >= scoreToNextLevel)
		{
			LevelUp();
		}
		score += Time.deltaTime * levelDifficulty;
		scoreText.text = Constants.Score + ((int)score).ToString();
		
	}

    private void LevelUp()
    {
		levelDifficulty++;
		scoreToNextLevel += scoreToUpgrade;
        GetComponent<PlayerMotor>().IncreaseSpeed();
		if(funFactManager != null)
		{
			funFactManager.DisplayFunFact();
		}
    }

	public void OnNotificationDisplay()
	{
		isFunFactDisplay = true;
	}

	public void OnNotificationEnd()
	{
		isFunFactDisplay = false;
	}

	public void OnDeath()
	{
		isDead = true;
		restartMenu.ToggleEndMenu(score);
	}

	public IEnumerator LevelFinished()
	{
		yield return new WaitForSeconds(1.5f);
		switch(FunFactManager.level)
		{
			case FunFactLevel.Level1:
				Application.LoadLevel(Constants.Levels.Actiuni);
				break;
			case FunFactLevel.Level2:
				Application.LoadLevel(Constants.Levels.Obligatiuni);
				break;
			case FunFactLevel.Level3:
				Application.LoadLevel(Constants.Levels.Derivative);
				break;
		}
	}
}
