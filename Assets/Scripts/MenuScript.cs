﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MenuScript : MonoBehaviour {
    public Canvas startMenu;
    public Canvas exitMenu;
    public Canvas credits;
    public Button startText;
    public Button exitText;
    public Button backText;


    // Use this for initialization
    void Start () {
        startMenu = startMenu.GetComponent<Canvas>();
        credits = credits.GetComponent<Canvas>();
        exitMenu = exitMenu.GetComponent<Canvas>();
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        backText = backText.GetComponent<Button>();
        exitMenu.enabled = false;
        credits.enabled = false;
    }

    public void ExitPress()
    {
        exitMenu.enabled = true;
        startText.enabled = false;
        exitText.enabled = false;
        credits.enabled = false;
        backText.enabled = false;
    }

    public void NoPress()
    {
        startMenu.enabled = true;
        exitMenu.enabled = false;
        startText.enabled = true;
        exitText.enabled = true;
        credits.enabled = false;
        backText.enabled = false;
    }

    public void Credits()
    {
        exitMenu.enabled = false;
        startText.enabled = false;
        exitText.enabled = false;
        startMenu.enabled = false;
        credits.enabled = true;
        backText.enabled = true;
    }

    public void StartLevel()
    {
        Application.LoadLevel(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
