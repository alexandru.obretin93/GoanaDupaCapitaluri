﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemorizeOrder : MonoBehaviour {
    public Canvas value;
    EnumActivitati.ActiuniQuizes myEnum;
    static string canvasName;

    // Use this for initialization
    void Start () {
        value = value.GetComponent<Canvas>();
        canvasName = value.name.ToString();
	}
	
	public static void LoadNext()
    {
        int i = -1;
        foreach (EnumActivitati.ActiuniQuizes foo in Enum.GetValues(typeof(EnumActivitati.ActiuniQuizes)))
        { 
            i++;
            if (foo.ToString().Equals(canvasName))
                break;

        }
        Application.LoadLevel(i + 1);
    }
}
