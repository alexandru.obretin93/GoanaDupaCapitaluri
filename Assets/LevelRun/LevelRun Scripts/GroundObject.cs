using UnityEngine;

[System.Serializable]
public class GroundObject
{
    public GameObject Ground;
    public Vector3 Offset;
    public GroundType GroundType;
}