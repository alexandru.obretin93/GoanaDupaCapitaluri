﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuizObligatiuni : MonoBehaviour
{

    //Q1 items
    public Image questionImage;
    public Canvas q2_obligatiuni_final;
    public Canvas q2_obligatiuni_start;
    public Canvas q2_questionCanvas;
    public Canvas q2_raspuns_corect;
    public Canvas q2_raspuns_gresit;
    public Button q2a1;
    public Button q2a2;
    public Button q2a3;
    public Button q2a4;

    public List<Quiz> listaIntrebari;
    public Quiz quizObligatiuni;
    private String intrebare;
    private List<Item> listaQuiz;
    private static int bondQuizNumber = 0;

    // Use this for initialization
    void Start()
    {
        GetQuizArray();

        questionImage = questionImage.GetComponent<Image>();
        questionImage.enabled = false;

        q2_raspuns_corect = q2_raspuns_corect.GetComponent<Canvas>();
        q2_raspuns_corect.enabled = false;

        q2_questionCanvas = q2_questionCanvas.GetComponent<Canvas>();
        q2_questionCanvas.enabled = false;

        q2_obligatiuni_final = q2_obligatiuni_final.GetComponent<Canvas>();
        q2_obligatiuni_final.enabled = false;

        q2_obligatiuni_start = q2_obligatiuni_start.GetComponent<Canvas>();
        q2_obligatiuni_start.enabled = true;

        q2_raspuns_gresit = q2_raspuns_gresit.GetComponent<Canvas>();
        q2_raspuns_gresit.enabled = false;

        q2_questionCanvas = q2_questionCanvas.GetComponent<Canvas>();

        q2a1 = q2a1.GetComponent<Button>();
        q2a2 = q2a2.GetComponent<Button>();
        q2a3 = q2a3.GetComponent<Button>();
        q2a4 = q2a4.GetComponent<Button>();

    }

    private void GetQuizArray()
    {
        listaIntrebari = new List<Quiz>();
        quizObligatiuni = new Quiz();
        listaQuiz = new List<Item>();
        //Item1
        intrebare = "Obligatiunea este";
        Item i1 = new Item(false, "parte a unui capital social");
        Item i2 = new Item(false, "un titlu de proprietate");
        Item i3 = new Item(true, "un titlu de credit");
        Item i4 = new Item(false, "un instrument bancar");
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare);

        listaIntrebari.Add(quizObligatiuni);

        //Item2
        string intrebare2 = "Cuponul";
        i1 = new Item(false, "este un venit variabil");
        i2 = new Item(true, "este un venit fix");
        i3 = new Item(false, "se refera la actiunile la purtator");
        i4 = new Item(false, "este asociat actiunilor nominative");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare2);

        listaIntrebari.Add(quizObligatiuni);

        //Item3
        string intrebare3 = "Obligatiunile sunt emise cu urmatorul scop";
        i1 = new Item(false, "mobilizarea de capital propriu");
        i2 = new Item(false, "cresterea imediata a veniturilor populatiei");
        i3 = new Item(false, "mobilizarea de capital pe termen lung");
        i4 = new Item(true, "mobilizarea de capital pe termen scurt");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare3);

        listaIntrebari.Add(quizObligatiuni);

        //Item4
        string intrebare4 = "Pentru o societate comerciala, posesorul unei obligatiuni emise este";
        i1 = new Item(false, "proprietar");
        i2 = new Item(true, "creditor");
        i3 = new Item(false, "debitor");
        i4 = new Item(false, "asociat");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare4);

        listaIntrebari.Add(quizObligatiuni);

        //Item5
        string intrebare5 = "Daca o societate comerciala emite obligatiuni noi";
        i1 = new Item(true, "datoriile societatii cresc");
        i2 = new Item(false, "capitalul social creste");
        i3 = new Item(false, "capitalul social se reduce");
        i4 = new Item(false, "datoriile societatii se reduc");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare5);

        listaIntrebari.Add(quizObligatiuni);

        //Item6
        string intrebare6 = "Daca o persoana cumpara o obligatiune din economiile proprii";
        i1 = new Item(false, "datoriile sale cresc");
        i2 = new Item(true, "activele persoanei cresc");
        i3 = new Item(false, "datoriile sale se reduc");
        i4 = new Item(false, "activele persoanei se reduc");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare6);

        listaIntrebari.Add(quizObligatiuni);

        //Item7
        string intrebare7 = "In general, gradul de risc al actiunilor este in raport cu gradul de risc al obligatiunilor";
        i1 = new Item(false, "mai mic");
        i2 = new Item(false, "egal");
        i3 = new Item(false, "nu exista o relatie de ordine intre cele doua");
        i4 = new Item(true, "mai mare");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare7);

        listaIntrebari.Add(quizObligatiuni);

        //Item8
        string intrebare8 = "Intre drepturile pe care le confera obligatiunea obligatarului nu se include";
        i1 = new Item(false, "dreptul de a incasa dobanzi");
        i2 = new Item(false, "dreptul de rambursare a imprumutului");
        i3 = new Item(false, "dreptul de a participa la AGA care se intruneste doar la cererea a cel putin un sfert din obligatari");
        i4 = new Item(true, "dreptul de vot in adunarea generala");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare8);

        listaIntrebari.Add(quizObligatiuni);

        //Item9
        string intrebare9 = "Obligatiunile sunt emise de";
        i1 = new Item(true, "agenti economici, pentru finantarea pe termen scurt");
        i2 = new Item(false, "stat, pentru finantarea golurilor de casa ce intervin in executia bugetului");
        i3 = new Item(false, "bancile comerciale, pentru procurarea resurselor financiare temporar disponibile");
        i4 = new Item(false, "orice persoana fizica sau juridica doritoare in acest scop");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare9);

        listaIntrebari.Add(quizObligatiuni);

        //Item10
        string intrebare10 = "Semnificatia valorii nominale a obligatiunii este urmatoarea";
        i1 = new Item(false, "reprezinta suma care, de regula, este rambursata la scadenta de catre emitent");
        i2 = new Item(false, "reprezinta valoarea dobanzii ce revine obligatarului");
        i3 = new Item(true, "reprezinta suma platita emitentului de viitorul creditor obligatar");
        i4 = new Item(false, " reprezinta suma in functie de care se stabileste cursul pietei");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizObligatiuni = new Quiz();
        quizObligatiuni.setLista(listaQuiz);
        quizObligatiuni.setIntrebare(intrebare10);

        listaIntrebari.Add(quizObligatiuni);
    }

    public void EvaluateAnswer()
    {
        GameObject selectedElement = EventSystem.current.currentSelectedGameObject;
        Text textContainer = selectedElement.GetComponentInChildren<Text>();
        string ut = textContainer.text;
        Quiz q = listaIntrebari[bondQuizNumber];
        List<Item> li = q.getLista();
        int i;
        for (i = 0; i < 4; i++)
        {
            if (ut.Equals(li[i].getValue()))
                break;
        }
        if (true == li[i].getType())
            CorrectButtonPress();
        else
            OtherButtonPress();
    }

    public void CorrectButtonPress()
    {
        bondQuizNumber++;
        q2_obligatiuni_final.enabled = false;
        q2_raspuns_corect.enabled = true;
        q2_obligatiuni_start.enabled = false;
        q2_questionCanvas.enabled = false;
        q2_raspuns_gresit.enabled = false;
        q2a1.enabled = false;
        q2a2.enabled = false;
        q2a3.enabled = false;
        q2a4.enabled = false;
    }

    public void OtherButtonPress()
    {
        q2_obligatiuni_final.enabled = false;
        q2_raspuns_gresit.enabled = true;
        q2_obligatiuni_start.enabled = false;
        q2_questionCanvas.enabled = false;
        q2_raspuns_corect.enabled = false;
        q2a1.enabled = false;
        q2a2.enabled = false;
        q2a3.enabled = false;
        q2a4.enabled = false;
    }

    public void Replay()
    {
        q2_obligatiuni_final.enabled = false;
        q2_raspuns_gresit.enabled = false;
        q2_questionCanvas.enabled = true;
        q2_raspuns_corect.enabled = false;
        q2_obligatiuni_start.enabled = false;
        q2a1.enabled = true;
        q2a2.enabled = true;
        q2a3.enabled = true;
        q2a4.enabled = true;
    }

    public void Next()
    {
        if (bondQuizNumber == 10)
        {
            bondQuizNumber++;

            q2_obligatiuni_final.enabled = true;
            q2_raspuns_gresit.enabled = false;
            q2_questionCanvas.enabled = false;
            q2_obligatiuni_start.enabled = false;
            q2_raspuns_corect.enabled = false;
            q2a1.enabled = false;
            q2a2.enabled = false;
            q2a3.enabled = false;
            q2a4.enabled = false;

            return;
        }

        if (bondQuizNumber == 11)
        {
            BondFading.bondLevelNotFinished = false;
            questionImage.enabled = false;
            q2_obligatiuni_final.enabled = false;
            q2_raspuns_gresit.enabled = false;
            q2_questionCanvas.enabled = false;
            q2_obligatiuni_start.enabled = false;
            q2_raspuns_corect.enabled = false;
            q2a1.enabled = false;
            q2a2.enabled = false;
            q2a3.enabled = false;
            q2a4.enabled = false;
            StartCoroutine(WaitMap());
            return;
        }

        q2_raspuns_corect = q2_raspuns_corect.GetComponent<Canvas>();
        q2_raspuns_corect.enabled = false;

        q2_obligatiuni_start = q2_obligatiuni_start.GetComponent<Canvas>();
        q2_obligatiuni_start.enabled = false;

        q2_raspuns_gresit = q2_raspuns_gresit.GetComponent<Canvas>();
        q2_raspuns_gresit.enabled = false;

        q2_questionCanvas = q2_questionCanvas.GetComponent<Canvas>();
        q2_questionCanvas.enabled = true;

        Text questionText = GameObject.Find("Q2Question").GetComponent<Text>();
        questionText.text = listaIntrebari[bondQuizNumber].getIntrebare();

        q2a1 = q2a1.GetComponent<Button>();
        q2a2 = q2a2.GetComponent<Button>();
        q2a3 = q2a3.GetComponent<Button>();
        q2a4 = q2a4.GetComponent<Button>();

        List<Item> Q1answers = listaIntrebari[bondQuizNumber].getLista();
        Text q2a1text = GameObject.Find("Q2A1Text").GetComponent<Text>();
        q2a1text.text = Q1answers[0].getValue();
        Text q2a2text = GameObject.Find("Q2A2Text").GetComponent<Text>();
        q2a2text.text = Q1answers[1].getValue();
        Text q2a3text = GameObject.Find("Q2A3Text").GetComponent<Text>();
        q2a3text.text = Q1answers[2].getValue();
        Text q2a4text = GameObject.Find("Q2A4Text").GetComponent<Text>();
        q2a4text.text = Q1answers[3].getValue();

        q2a1.enabled = true;
        q2a2.enabled = true;
        q2a3.enabled = true;
        q2a4.enabled = true;
    }

    IEnumerator WaitMap()
    {
        yield return new WaitForSeconds(1.5f);
        Application.LoadLevel("Map");
    }
}
