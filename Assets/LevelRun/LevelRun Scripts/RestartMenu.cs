﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class RestartMenu : MonoBehaviour {

	public Text scoreText;
	public Image backgroundImage;
	private bool isShowned = false;
	private float transition = 0.0f;
	public float opacity;

	// Use this for initialization
	void Start () {
		gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(!isShowned)
			return;
		transition += Time.deltaTime/3;
		if(transition < opacity)
			backgroundImage.color = Color.Lerp(new Color(0,0,0,0), Color.black, transition);
	}

	public void ToggleEndMenu(float score)
	{
		isShowned = true;
		gameObject.SetActive(true);
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
	}
}
