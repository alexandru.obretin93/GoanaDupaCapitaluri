﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {
	public class Tags{
		public const string Player = "MariaPlayer";
		public const string Water = "Water";
		public const string GeneratePoint = "PointToGenerate";
		public const string DestroyPoint = "PointToDestroy";
		public const string GroundUp = "GroundUp";
		public const string GroundDown = "GroundDown";
		public const string GroundStraight = "GroundStraight";
		public const string FunFactInfo = "FunFactInfo";
		public const string ScoreText = "ScoreText";
		
		
	}
	public class Animations{
		public const string Jump = "Jump";
		public const string Die = "Die";
	}

	public class GroundTypes
	{
		public const string Straight_Down = "Straight_Down";
		public const string Straight_Up = "Straight_Up";
		public const string All = "All";
		
	}

	public class Levels
	{
		public const string Actiuni = "Q1_Actiuni";
		public const string Obligatiuni = "Q2_Obligatiuni";
		public const string Derivative = "Q3_Derivative";
		
	}

	public const string Score = "Scor: ";

	public const string DidYouKnow = "Stiai ca?";

	public static Dictionary<FunFactLevel, string[]> FunFacts = new Dictionary<FunFactLevel, string[]>
	{
		{
			FunFactLevel.Level1, 
			new string[]
			{
				"Bun venit in <b>Tara Pietelor de Capital</b>. Esti pregatit de aventura? \n\t Ajut-o pe <b>Maria</b> sa ajunga la finalul drumului si sa acumuleze cat mai multe cunostinte despre pietele de capital.",
				"Motorul economiei de piata este reprezentat de <b>agentii economici</b>(firme). Dintre acestea, un rol important il au <b>societatile comerciale pe actiuni</b>",				
				"Societatea pe actiuni(<b>SA</b>) reprezinta o intreprindere al carui capital social este impartit in actiuni. Acestea pot fi detinute de una sau mai multe persoane.",
				"Societatile pe actiuni pot emite <b>titluri de capital</b> ce pot fi achizitionate de <b>oricine</b>.",
				"Actiunile rezultate in urma <b>primei emisiuni</b> vor fi tranzactionate pe piata principala. O vanzare ulterioara va avea loc pe piata sedundara",
				"Mai exact, o actiune reprezinta o parte din capitalul unei societăți, <b>divizat in parti egale</b>.",
				"In urma achizitiei de titluri de proprietate, cumparatorul primeste un document<i>(in forma materializata)</i> ce atesta numarul de actiuni detinute.",
				"Documentul ce atesta titlul de proprietate poate contine numele detinatorului sau nu. In acest ultim caz, actiunile <b>nu sunt legate</b> de o persoana si pot fi tranzactionate liber.",
				"Titlurile de proprietate ce contin numele proprietarului se numesc <b>nominale</b> iar celalate, <b>actiuni la purtator</b><i>(cine le detine este proprietarul)</i>.",
				"Evidenta tuturor proprietarilor de actiuni nominative este consemnata intr-un <b>Registru al Actionarilor</b>",
				"Dupa cumpararea actiunilor, proprietarul le poate pastra sau vinde. Daca alege sa le mentina, acesta poate primi <b>dividente</b>.",
				"<b>Dividendul</b> reprezinta un venit variabil ce poate fi obtinut in urma detinerii de actiuni.",
				"Este important de retinut faptul ca venitul adus de dividente nu este unul fix, el fiind calculat in functie de performantele intreprinderii intr-un an.",
				"Pe langa dividente, persoanele ce detin actiuni pot participa si vota la <b>Adunarea Generala a Actionarilor (AGA)<>, sedinta in care se iau decizii cu privire la modul de gestionare a resurselor companiei.",
				"AGA este cea care stabileste De asemenea, este posibil ca intr-un an sa nu fie acordate venituri daca asa hotaraste AGA.",
				"In cazul in care compania doreste sa isi <b>rascumpere actiunile</b>, ea este nevoita sa achite valoarea nominala inscrisa pe titluri.",
				"De multe ori, compania se afla in situatia in care doreste sa stranga un capital mai mare pentru investitii. O solutie o reprezinta <b>emiterea de actiuni</b> pentru atragerea de fonduri.",
			}
		}, 
		{
			FunFactLevel.Level2, 
			new string[]
			{
				"Nevoia agentilor economici de a obtine fonduri pe termen scurt a dus la aparitia unui nou instrument financiar.",
				"<b>Obligatiunea</b> reprezinta un titlu de credit care spre deosebire de actiuni aduce un venit fix.",
				"Daca in cazul actiunilor valoarea nominala reprezinta o cota parte din capitalul social, in cazul obligatiunilor reprezinta suma imprumutata.",
				"Cumparatorul obligatiunii are la data scadenta inscrisa pe titlu <b>dreptul</b> de rambursare a sumei acordate precum si a unei dobanzi, cupon.",
				"Similar cu actiunea obligatiunea confera drept de participare in AGA <b>dar</b> nu si de vot.",
				"Prin emiterea de obligatiuni incearca atragerea de capital pe termen scurt cu scopul de a il reinvisti.",
				"Obligatiunile reprezinta o investitie pe <b>termen mediu</b> si lung ce contribuie la sporirea activelor obligatorului.",
				"In general, obligatiunile aduc un castig mai mic decat actiunile dar sunt mai sigure. Investitiile in actiuni pot duce la pierderi.",
				"Printre cele mai sigure obligatiuni sunt cele guvernamentale, cunoscute si sub numele de <b>titluri de stat</b>"
			}
		},
		{
			FunFactLevel.Level3,
			new string[]
			{
				"Economia de piata se afla intr-o evolutie continua ca urmare a schimbarilor sociale si tehnologice continue. Acest lucru a dus in anii 1990 la aparitia unor instrumente financiare complexe.",
				"<b>Instrumentelor derivate</b> au aparut din nevoia de a te proteja impotriva riscului financiar, dar si din alte motive speculative. Acestea sprijina transferul riscului de la entitate la alta.",
				"Aceste instrumente financiare complexe deriva din alte active cu rol de suport. Aceste active suport pot fi: actiuni, obligatiuni, monede, indecsi bancari sau bunuri.",
				"Ganditi-va ca vreti sa va abonati la cablu. Va intelegeti cu compania ca in urmatorul an sa aveti in fiecare luna acces la anumite canale la un pret prestabilit. Acesta este un <b>contract futures</b>",
				"O alternativa la contractele futures o reprezinta cele <b>forward</b>. Pentru a intelege ce presupun sa consideram urmatorul exemplu: Maria detine o ferma ce produce cereale...",
				"In primavara ea se teme ca pretul cerealelor va scadea pana la recoltare. Pentru a se proteja impotriva acestui risc Maria negociaza un contract forward prin cumparatorul se angajeaza sa cumpere la un pret prestabilit.",
				"Astfel, daca Maria are dreptate, cumparatorul contractului ii va plati diferenta, insa daca pretul va fi peste cel asteptat, atunci Maria va trebui sa ofere castigul suplimentar cumparatorului.",
				"<b>Diferenta</b> intre contractul future si cel forward este in standardizarea acestora. Contractele future sunt standardizate si reglementate, pe cand cele forward sunt intelegeri directe intre parti, de unde si riscul mai ridicat. ",
				"Un contract de tip <b>SWAP</b> este un contract derivat prin care doua entitati schimba intre ele instrumente financiare. Acestea convin asupra unei <b>date fixe</b> la care tranzactia va avea loc.",
				"Deoarece aceste instrumente in general au un grad de risc mai ridicat, creditorii se folosesc de <b>Credit Default Swap</b> pentru a se feri de evenimente nepravazute(faliment, intarzierea platilor)",
				"Sa ne imaginam ca Maria detine o obligatiune pe 10 ani in valoare de 1000 de lei cu o dobanda anuala de 100 de lei, insa se teme ca firma care a emis obligatiunea nu va fi capabila sa o plateasca.",
				"In acest caz, ea se intelege cu George sa ii plateasca 20% din castig in schimbul unei asigurari ca in cazul in care firma este incapabila de plata sa ii rascumpere investitia. ",
			}
		}
	};

	public struct Information{
		public string Info;
		public bool wasDisplayed;
	}
}
