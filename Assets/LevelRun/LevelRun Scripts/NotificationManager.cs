﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NotificationManager : MonoBehaviour {
	private static Texture texture;
	public Texture notificationTexture;
	private static Texture buttonTexture;
	public Texture buttonOkTexture;
	public static float timer = 0.0f;
	private static bool callNotification = false;
	private static string message;
	public static GUIStyle textStyle;
	public GUIStyle myStyle;

	public static Vector2 notificationSize;
	public Vector2 startinPos,endPos,size;
	public Vector2 buttonPos;
	private static Vector2 buttonPosition;
	public static Vector2 startingPosition;
	public static Vector2 wantedPosition;
	public static Vector2 currentPos;
	private static Vector3 velocity3;
	public AudioSource ourSound;
	public static AudioSource audioSource;
	private static Vector2 okButtonPosition;
	private static int buttonSize;
	private static bool drawNotification;

	void Awake(){
		audioSource = ourSound;
		startingPosition = startinPos;
		wantedPosition = endPos;
		currentPos = startingPosition;
		texture = notificationTexture;
		buttonTexture = buttonOkTexture;
		textStyle = myStyle;
		buttonPosition = buttonPos;
		buttonSize = Screen.width/6;
		okButtonPosition =  new Vector2(Screen.width - Screen.width/2 - buttonSize/2, Screen.height - Screen.height/4 - buttonSize/4);
		drawNotification = false;


		notificationSize = new Vector2(buttonSize*4, buttonSize*2);
		textStyle.fixedWidth = notificationSize.x/2 + buttonSize * 3/4;
		textStyle.fixedHeight = notificationSize.y/2 ;
		textStyle.fontSize = Convert.ToInt32(textStyle.fixedWidth / textStyle.fixedHeight) * 12 - 2;
	}
	private  static void pushNotification(string message){
		audioSource.Play();
		currentPos = Vector3.SmoothDamp(currentPos, wantedPosition, ref velocity3, 0.35f);
		GUI.DrawTexture(new Rect(currentPos.x,currentPos.y,notificationSize.x,notificationSize.y),texture);

		GUI.Box(new Rect(currentPos.x+buttonSize,currentPos.y + buttonSize/3,notificationSize.x-buttonSize/2,notificationSize.y-buttonSize),message.ToString(),textStyle);
		if(GUI.Button(new Rect(okButtonPosition.x, okButtonPosition.y, buttonSize, buttonSize), buttonTexture, GUIStyle.none))
		{
			DismissNotification();
		}
		drawNotification = true;
	}

	private static void DismissNotification()
	{
		currentPos = Vector3.SmoothDamp(currentPos, startingPosition, ref velocity3, 0.35f);		
		notificationsList.RemoveAt(0);
		drawNotification = false;
		GameObject.FindGameObjectWithTag(Constants.Tags.FunFactInfo).GetComponent<FunFactManager>().DismissFunFact();
	}
	private static List<string> notificationsList = new List<string>();
	
	public static void CallNotification(string _message){
		notificationsList.Add(_message);
		drawNotification = true;
		//pushNotification(notificationsList[0]);
	}

	void OnGUI(){
		if(drawNotification && notificationsList.Count > 0)
		{
			pushNotification(notificationsList[0]);
		}
	}
}