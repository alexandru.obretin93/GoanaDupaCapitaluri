﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiz : MonoBehaviour {
    private string intrebare;
    private List<Item> listaQuiz = new List<Item>();

    public string getIntrebare()
    {
        return intrebare;
    }

    public void setIntrebare(string s)
    {
        intrebare = s;
    }

    public List<Item> getLista()
    {
        return listaQuiz;
    }

    public void setLista(List<Item> l)
    {
        listaQuiz = l;
    }

    public Quiz()
    {
        this.intrebare = "";
        this.listaQuiz = new List<Item>();
    }

}

public class Item
{
    private bool isCorrect;
    private string value;

    public bool getType()
    {
        return isCorrect;
    }

    public void setType(bool t)
    {
        isCorrect = t;
    }

    public string getValue()
    {
        return value;
    }

    public void setValue(string s)
    {
        value = s;
    }

    public Item(bool b, string s)
    {
        isCorrect = b;
        value = s;
    }
}
