﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GroundManager : MonoBehaviour {

	private Transform playerTransform;
	private Transform groundManagerTransform;
	public GroundObject[] GroundPrefabs;
	public GroundObject[] straightUpPrefabs;
	public GroundObject[] straightDownPrefabs;
	private GameObject generatePoint;
	private GameObject destroyPoint;
	private string nextGroundType = Constants.GroundTypes.Straight_Up;
	public float distanceToProject;

	// Use this for initialization
	void Start () {
		playerTransform = GameObject.FindGameObjectWithTag(Constants.Tags.Player).transform;
		groundManagerTransform = GetComponent<Transform>();
		straightDownPrefabs = GroundPrefabs.Where(x => x.GroundType != GroundType.Up).ToArray();
		straightUpPrefabs = GroundPrefabs.Where(x => x.GroundType != GroundType.Down).ToArray();
	}
	
	// Update is called once per frame
	void Update () {
		generatePoint = GameObject.FindGameObjectWithTag(Constants.Tags.GeneratePoint);
		destroyPoint = GameObject.FindGameObjectWithTag(Constants.Tags.DestroyPoint);
		
		if(Vector3.Distance(playerTransform.position, generatePoint.transform.position) < distanceToProject)
		{
			SpawnGround();
		}
		if(Vector3.Distance(destroyPoint.transform.position, playerTransform.transform.position) > distanceToProject * 2)
		{
			Destroy(destroyPoint.transform.parent.gameObject);
		}
	}

	private void SpawnGround()
	{
		GameObject go;
		var instantiatedPrefab = RandomPrefabIndex();
		go = Instantiate(instantiatedPrefab.Ground as GameObject);
		go.transform.SetParent(groundManagerTransform);
		go.transform.position = generatePoint.transform.position + instantiatedPrefab.Offset;
		Destroy(generatePoint);
	}

	private GroundObject RandomPrefabIndex()
	{
		if(GroundPrefabs.Length <= 1)
			{
				return GroundPrefabs[0];
			}
		var prefab = new GroundObject();
		{
			switch(nextGroundType){
				case Constants.GroundTypes.Straight_Up:
					prefab = straightUpPrefabs[UnityEngine.Random.Range(0, straightUpPrefabs.Length)];
					if(prefab.GroundType == GroundType.Up)
					{
						nextGroundType = Constants.GroundTypes.Straight_Down;
					}
					break;
				case Constants.GroundTypes.Straight_Down:
					prefab = straightDownPrefabs[UnityEngine.Random.Range(0, straightDownPrefabs.Length)];
					if(prefab.GroundType == GroundType.Down)
					{
						nextGroundType = Constants.GroundTypes.Straight_Up;
					}
					break;
				default:
					prefab = GroundPrefabs[UnityEngine.Random.Range(0, GroundPrefabs.Length)];
					break;
			}
		}
		return prefab;
	}
}
