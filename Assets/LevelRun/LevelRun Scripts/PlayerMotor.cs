﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour {

	private Vector3 moveVector;
	private CharacterController controller;
	private Transform playerTransform;
	public float speed;
	public float speedDuringFunFact;
	private float speedToResume;
	private float animatorSpeedToResume;
	private float verticalVelocity ;
	public float gravityForce;
	public float freezeTime;
	private Animator playerAnimator;
	private int jumpHash;
	private int dieHash;
	public float jumpForce;
	private float jumpForceOnPause;
	public float tilt;
	private bool isDying = false;
	private bool isDead = false;
	private AudioSource jumpSound;
	
	public float animatorIncreaseSpeedAmount;
	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController>();
		jumpSound = GetComponent<AudioSource>();
		playerAnimator = GetComponent<Animator>();
		jumpHash = Animator.StringToHash(Constants.Animations.Jump);
		dieHash = Animator.StringToHash(Constants.Animations.Die);
		playerTransform = GetComponent<Transform>();
	}

    // Update is called once per frame
    private void Update () {
		if(isDying)
		{
			if(!controller.isGrounded)
			{
				controller.Move(Vector3.down);
				return;
			}
			if(!isDead)
				controller.Move(Vector3.back/2);
			isDead = true;
			return;
		}

		moveVector = Vector3.zero;
		if(Time.time < freezeTime){
			controller.Move(Vector3.forward * Time.deltaTime * speed); 
			return;
		}

		if(controller.isGrounded && speed > animatorIncreaseSpeedAmount)
		{
			verticalVelocity = 0.0f;

            if (Input.touches.Length > 0 && Input.GetTouch(0).deltaPosition.y > 0)
			{
				playerAnimator.SetTrigger(jumpHash);
				jumpSound.Play();
				verticalVelocity = gravityForce * jumpForce;
			}
            else
			{
				playerAnimator.ResetTrigger(jumpHash);
			}
		}
		else
		{
			verticalVelocity -= gravityForce * 3 * Time.deltaTime;
		}
        //X - left and right
        moveVector.x = Input.acceleration.x;

        //moveVector.x =  Input.GetAxisRaw("Horizontal");
		//Y - up and down
		moveVector.y = verticalVelocity;
		//Z - foward and backward
		moveVector.z = speed;
		controller.Move((moveVector * speed )* Time.deltaTime);
		playerTransform.rotation = Quaternion.Euler(moveVector.x * -tilt, 0, moveVector.x * -tilt);
	}

	public void IncreaseSpeed()
	{
		speed += animatorIncreaseSpeedAmount;
		playerAnimator.speed += animatorIncreaseSpeedAmount;
	}

	public void OnNotificationDisplay()
	{
		//reduce player speed
		speedToResume = speed;
		jumpForceOnPause = jumpForce;
		jumpForce = jumpForce * 2.5f;
		animatorSpeedToResume = playerAnimator.speed;
		speed = speedDuringFunFact;
		playerAnimator.speed = speedDuringFunFact;
	}

	public void OnNotificationEnd()
	{
		speed = speedToResume;
		jumpForce = jumpForceOnPause;
		playerAnimator.speed = animatorSpeedToResume;
	}

	public void KillPlayer()
	{
		isDying = true;
		playerAnimator.ResetTrigger(jumpHash);
		playerAnimator.SetTrigger(dieHash);
		GetComponent<Score>().OnDeath();
	}

	
}
