﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuizActiuni : MonoBehaviour
{
    //Q1 items
    public Image questionImage;
    public Canvas q1_actiuni_final;
    public Canvas q1_actiuni_start;
    public Canvas q1_questionCanvas;
    public Canvas q1_raspuns_corect;
    public Canvas q1_raspuns_gresit;
    public Button q1a1;
    public Button q1a2;
    public Button q1a3;
    public Button q1a4;

    public List<Quiz> listaIntrebari;
    public Quiz quizActiuni;
    private String intrebare;
    private List<Item> listaQuiz;
    private static int quizNumber = 0;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        GetQuizArray();

        questionImage = questionImage.GetComponent<Image>();
        questionImage.enabled = false;

        q1_questionCanvas = q1_questionCanvas.GetComponent<Canvas>();
        q1_questionCanvas.enabled = false;

        q1_actiuni_start = q1_actiuni_start.GetComponent<Canvas>();
        q1_actiuni_start.enabled = true;

        q1_raspuns_corect = q1_raspuns_corect.GetComponent<Canvas>();
        q1_raspuns_corect.enabled = false;

        q1_raspuns_gresit = q1_raspuns_gresit.GetComponent<Canvas>();
        q1_raspuns_gresit.enabled = false;

        q1_actiuni_final = q1_actiuni_final.GetComponent<Canvas>();
        q1_actiuni_final.enabled = false;

        q1_questionCanvas = q1_questionCanvas.GetComponent<Canvas>();

        q1a1 = q1a1.GetComponent<Button>();
        q1a1.enabled = false;
        q1a2 = q1a2.GetComponent<Button>();
        q1a2.enabled = false;
        q1a3 = q1a3.GetComponent<Button>();
        q1a3.enabled = false;
        q1a4 = q1a4.GetComponent<Button>();
        q1a4.enabled = false;
    }

    private void GetQuizArray()
    {
        listaIntrebari = new List<Quiz>();
        quizActiuni = new Quiz();
        listaQuiz = new List<Item>();
        //Item1
        intrebare = "Actiunile sunt";
        Item i1 = new Item(false, "titluri de creanta");
        Item i2 = new Item(false, "parti ale bugetului de stat");
        Item i3 = new Item(true, "parti ale capitalului social");
        Item i4 = new Item(false, "instrumente bancare, precum cardurile");
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare);

        listaIntrebari.Add(quizActiuni);

        //Item2
        string intrebare2 = "Dividendul";
        i1 = new Item(false, "este un venit fix");
        i2 = new Item(true, "este un venit variabil");
        i3 = new Item(false, "depinde de marimea firmei");
        i4 = new Item(false, "este aferent actiunilor si obligatiunilor");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare2);

        listaIntrebari.Add(quizActiuni);

        //Item3
        string intrebare3 = "Actiunile pot fi cumparate de";
        i1 = new Item(false, "cetatenii straini");
        i2 = new Item(false, "cei care sunt deja actionari");
        i3 = new Item(false, "persoane special imputernicite");
        i4 = new Item(true, "oricine");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare3);

        listaIntrebari.Add(quizActiuni);

        //Item4
        string intrebare4 = "Valoarea nominala a actiunii reprezinta suma";
        i1 = new Item(false, "pe care o plateste cel care subscrie");
        i2 = new Item(true, " pe care trebuie sa o plateasca emitentul pentru a-si rascumpara actiunile");
        i3 = new Item(false, "in functie de care se stabilesc drepturile de vot");
        i4 = new Item(false, "in functie de care se stabileste venitul actionarului");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare4);

        listaIntrebari.Add(quizActiuni);

        //Item5
        string intrebare5 = "Care dintre urmatoarele nu este adevarata cu privire la actiuni";
        i1 = new Item(true, "sunt fractiuni inegale ale capitalului social");
        i2 = new Item(false, "sunt titluri negociabile");
        i3 = new Item(false, "se pot transmite liber altor persoane");
        i4 = new Item(false, "pot fi tranzactionate pe piete reglementate");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare5);

        listaIntrebari.Add(quizActiuni);

        //Item6
        string intrebare6 = "Actiunile la purtator se pot emite in urmatoarea forma";
        i1 = new Item(false, "dematerializata");
        i2 = new Item(true, "materializata");
        i3 = new Item(false, "materializata sau dematerializata");
        i4 = new Item(false, "niciuna din variante");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare6);

        listaIntrebari.Add(quizActiuni);

        //Item7
        string intrebare7 = "Daca o societate comerciala emite actiuni noi, atunci";
        i1 = new Item(false, "datoriile societatii cresc");
        i2 = new Item(false, "datoriile societatii se reduc");
        i3 = new Item(false, "capitalul social se reduce");
        i4 = new Item(true, "capitalul social creste");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare7);

        listaIntrebari.Add(quizActiuni);

        //Item8
        string intrebare8 = "Intre drepturile pe care le confera actiunea posesorului acesteia nu se include";
        i1 = new Item(false, "dreptul de a primi dividende");
        i2 = new Item(false, "dreptul de vot in adunarea generala");
        i3 = new Item(false, "dreptul de a participa la AGA");
        i4 = new Item(true, "dreptul de a incasa dobanzi");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare8);

        listaIntrebari.Add(quizActiuni);

        //Item9
        string intrebare9 = "Care dintre afirmatiile urmatoare este falsa";
        i1 = new Item(true, "actiunea aduce un venit fix");
        i2 = new Item(false, "actiunea este un titlu de proprietate");
        i3 = new Item(false, "venitul actiunii este dividendul");
        i4 = new Item(false, "actiunea exista atat timp cat functioneaza societatea emitenta");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare9);

        listaIntrebari.Add(quizActiuni);

        //Item10
        string intrebare10 = "Actiunile nominative se caracterizeaza prin";
        i1 = new Item(false, "posesia titlului e necesara si suficienta pt exercitarea drepturilor actionarului");
        i2 = new Item(false, "confera detinatorului un drept preferential in cazul lichidarii societatii");
        i3 = new Item(true, "proprietarii acestor actiuni sunt inscrisi intr-un Registru al Actionarilor");
        i4 = new Item(false, "detinatorul lor este considerat actionarul legitim");
        listaQuiz = new List<Item>();
        listaQuiz.Add(i1);
        listaQuiz.Add(i2);
        listaQuiz.Add(i3);
        listaQuiz.Add(i4);
        quizActiuni = new Quiz();
        quizActiuni.setLista(listaQuiz);
        quizActiuni.setIntrebare(intrebare10);

        listaIntrebari.Add(quizActiuni);
    }

    public void EvaluateAnswer()
    {
        GameObject selectedElement = EventSystem.current.currentSelectedGameObject;
        Text textContainer = selectedElement.GetComponentInChildren<Text>();
        string ut = textContainer.text;
        Quiz q = listaIntrebari[quizNumber];
        List<Item> li = q.getLista();
        int i;
        for (i = 0; i < 4; i++)
        {
            if (ut.Equals(li[i].getValue()))
                break;
        }
        if (true == li[i].getType())
            CorrectButtonPress();
        else
            OtherButtonPress();
    }

    public void CorrectButtonPress()
    {
        quizNumber++;
        q1_raspuns_corect.enabled = true;
        q1_questionCanvas.enabled = false;
        q1_raspuns_gresit.enabled = false;
        q1_actiuni_final.enabled = false;
        q1_actiuni_start.enabled = false;
        q1a1.enabled = false;
        q1a2.enabled = false;
        q1a3.enabled = false;
        q1a4.enabled = false;
    }

    public void OtherButtonPress()
    {
        q1_actiuni_final.enabled = false;
        q1_raspuns_gresit.enabled = true;
        q1_actiuni_start.enabled = false;
        q1_questionCanvas.enabled = false;
        q1_raspuns_corect.enabled = false;
        q1a1.enabled = false;
        q1a2.enabled = false;
        q1a3.enabled = false;
        q1a4.enabled = false;
    }

    public void Replay()
    {
        q1_actiuni_final.enabled = false;
        q1_actiuni_start.enabled = false;
        q1_raspuns_gresit.enabled = false;
        q1_questionCanvas.enabled = true;
        q1_raspuns_corect.enabled = false;
        q1a1.enabled = true;
        q1a2.enabled = true;
        q1a3.enabled = true;
        q1a4.enabled = true;
    }

    public void Next()
    {
        if (quizNumber == 10)
        {
            quizNumber++;

            q1_actiuni_start.enabled = false;
            q1_actiuni_final.enabled = true;
            q1_raspuns_gresit.enabled = false;
            q1_questionCanvas.enabled = false;
            q1_raspuns_corect.enabled = false;
            q1a1.enabled = false;
            q1a2.enabled = false;
            q1a3.enabled = false;
            q1a4.enabled = false;

            return;
        }

        if (quizNumber == 11)
        {
            q1_actiuni_start.enabled = false;
            questionImage.enabled = false;
            q1_actiuni_final.enabled = false;
            q1_raspuns_gresit.enabled = false;
            q1_questionCanvas.enabled = false;
            q1_raspuns_corect.enabled = false;
            q1a1.enabled = false;
            q1a2.enabled = false;
            q1a3.enabled = false;
            q1a4.enabled = false;
            SharesFading.sharesLevelNotFinished = false;
            StartCoroutine(WaitMap());
            return;
        }
        q1_actiuni_start = q1_actiuni_start.GetComponent<Canvas>();
        q1_actiuni_start.enabled = false;

        q1_raspuns_corect = q1_raspuns_corect.GetComponent<Canvas>();
        q1_raspuns_corect.enabled = false;

        q1_raspuns_gresit = q1_raspuns_gresit.GetComponent<Canvas>();
        q1_raspuns_gresit.enabled = false;

        q1_questionCanvas = q1_questionCanvas.GetComponent<Canvas>();
        q1_questionCanvas.enabled = true;

        Text questionText = GameObject.Find("Q1Question").GetComponent<Text>();
        questionText.text = listaIntrebari[quizNumber].getIntrebare();

        q1a1 = q1a1.GetComponent<Button>();
        q1a2 = q1a2.GetComponent<Button>();
        q1a3 = q1a3.GetComponent<Button>();
        q1a4 = q1a4.GetComponent<Button>();

        List<Item> Q1answers = listaIntrebari[quizNumber].getLista();
        Text q1a1text = GameObject.Find("Q1A1Text").GetComponent<Text>();
        q1a1text.text = Q1answers[0].getValue();
        Text q1a2text = GameObject.Find("Q1A2Text").GetComponent<Text>();
        q1a2text.text = Q1answers[1].getValue();
        Text q1a3text = GameObject.Find("Q1A3Text").GetComponent<Text>();
        q1a3text.text = Q1answers[2].getValue();
        Text q1a4text = GameObject.Find("Q1A4Text").GetComponent<Text>();
        q1a4text.text = Q1answers[3].getValue();

        q1a1.enabled = true;
        q1a2.enabled = true;
        q1a3.enabled = true;
        q1a4.enabled = true;
    }

    IEnumerator WaitMap()
    {
        yield return new WaitForSeconds(1.5f);
        Application.LoadLevel("Map");
    }
}
