﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumActivitati : MonoBehaviour {
    public enum ActiuniQuizes
    {
        StartMenu, Map,

        Actiuni_Q1_Actiune, Actiuni_Q2_Dividend, Actiuni_Q3_x, Actiuni_Q4_x, Actiuni_Q5_x,
        Actiuni_Q6_x, Actiuni_Q7_x, Actiuni_Q8_x, Actiuni_Q9_x, Actiuni_Q10_x,

        Dividende_Q1_Actiune, Dividende_Q2_Dividend, Dividende_Q3_x, Dividende_Q4_x, Dividende_Q5_x,
        Dividende_Q6_x, Dividende_Q7_x, Dividende_Q8_x, Dividende_Q9_x, Dividende_Q10_x

        //Dividende_Q1_Actiune, Dividende_Q2_Dividend, Dividende_Q3_x, Dividende_Q4_x, Dividende_Q5_x,
        //Dividende_Q6_x, Dividende_Q7_x, Dividende_Q8_x, Dividende_Q9_x, Dividende_Q10_x,

        //Dividende_Q1_Actiune, Dividende_Q2_Dividend, Dividende_Q3_x, Dividende_Q4_x, Dividende_Q5_x,
        //Dividende_Q6_x, Dividende_Q7_x, Dividende_Q8_x, Dividende_Q9_x, Dividende_Q10_x,
    }
}
