﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatAndCameraPos : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(Ship.shipV.x != -1 && Ship.shipV.y != -1 && Ship.shipV.z != -1)
        {
            if (SharesFading.sharesLevelNotFinished == false)
            {
                TextMesh treiDText = GameObject.Find("PressF").GetComponent<TextMesh>();
                if (string.Empty.CompareTo(treiDText.text) != 0)
                {
                    treiDText.text = "";
                }
            }

            if (BondFading.bondLevelNotFinished == false)
            {
                TextMesh treiDText = GameObject.Find("PressBondF").GetComponent<TextMesh>();
                if (string.Empty.CompareTo(treiDText.text) != 0)
                {
                    treiDText.text = "";
                }
            }

            GameObject ship = GameObject.Find("Ship");
            ship.transform.position = Ship.shipV;

            GameObject camera = GameObject.Find("MultipurposeCameraRig");
            camera.transform.position = Ship.shipC;
        }
    }
}
