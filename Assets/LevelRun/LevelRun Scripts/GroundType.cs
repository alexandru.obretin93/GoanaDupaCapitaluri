﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum GroundType {
	Up = 1,
	Down = 2,
	Straight = 3
}

