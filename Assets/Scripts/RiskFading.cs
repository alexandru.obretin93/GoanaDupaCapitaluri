﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RiskFading : MonoBehaviour {

    public static RiskFading Instance { get; set; }
    public static bool launchFading = false;
    public Image fadeImage;
    private bool isInTransition;
    private float transition;
    private bool isShowing;
    private float duration;
    public static bool riskLevelNotFinished = true;

    private void Awake()
    {
        Instance = this;
    }

    public void Fade(bool showing, float duration)
    {
        isShowing = showing;
        isInTransition = true;
        this.duration = duration;
        transition = (isShowing) ? 0 : 1;
    }

    void OnTriggerEnter(Collider other)
    {
        launchFading = true;

        TextMesh treiDText = GameObject.Find("PressRiskF").GetComponent<TextMesh>();
        treiDText.text = "Apasa tasta F";
    }

    private void Update()
    {
        if (Input.GetKey("f") && launchFading == true && riskLevelNotFinished == true)
        {
            Fade(true, 1.25f);
        }

        if (!isInTransition)
            return;

        transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
        fadeImage.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, transition);

        if (transition > 1 || transition < 0)
            isInTransition = false;

        Ship.shipV = new Vector3(Ship.boatxPos, Ship.boatyPos, Ship.boatzPos);
        Ship.shipC = new Vector3(Ship.cameraxPos, Ship.camerayPos, Ship.camerazPos);

        StartCoroutine(WaitRisk());
    }


    IEnumerator WaitRisk()
    {
        Debug.Log("Initial pause: " + Time.time);
        yield return new WaitForSeconds(3.0f);
        Debug.Log("After pause: " + Time.time);
        Application.LoadLevel("Q4_Risk");
    }
}
