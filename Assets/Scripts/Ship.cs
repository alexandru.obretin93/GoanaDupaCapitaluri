﻿  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
    public static Vector3 shipV = new Vector3(-1,-1,-1);
    public static float boatxPos;
    public static float boatyPos;
    public static float boatzPos;

    private GameObject camera;
    public static Vector3 shipC = new Vector3(-1, -1, -1);
    public static float cameraxPos;
    public static float camerayPos;
    public static float camerazPos;

    private Rigidbody rbody;
    public float turnSpeed = 100f;
    public float accelerateSpeed = 100f;

	// Use this for initialization
	void Start () {
        rbody = GetComponent<Rigidbody>();
        camera = GameObject.Find("MultipurposeCameraRig");
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        boatxPos = transform.position.x;
        boatyPos = transform.position.y;
        boatzPos = transform.position.z;
        Debug.Log("boat pos x: " + boatxPos);


        cameraxPos = camera.transform.position.x;
        camerayPos = camera.transform.position.y;
        camerazPos = camera.transform.position.z;
        rbody.AddTorque(0f, Input.acceleration.x * turnSpeed * Time.deltaTime, 0f);

        Debug.Log("y pos: " + (Input.acceleration.y + 0.5f));

        rbody.AddForce(transform.forward * (Input.acceleration.y + 0.5f) * accelerateSpeed * Time.deltaTime);

    }
}
