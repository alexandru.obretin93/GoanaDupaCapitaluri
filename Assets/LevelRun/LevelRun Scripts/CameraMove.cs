﻿
using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

	private Transform playerTransform;
	private Vector3 startOffset;
	private Transform cameraTransform;
	private Vector3 moveVector;
	public float minCameraHeight;
	public float maxCameraHeight;
	public float xCameraPosition;
	public float yWavePosition;
	
	private float transition = 0.0f;
	public float startAnimationDuration;
	public Vector3 animationOffset;
	private Transform waterTransform;
	private Vector3 waterOffset;
	// Update is called once per frame
	void Start () 
	{
		cameraTransform = GetComponent<Transform>();
		playerTransform = GameObject.FindGameObjectWithTag(Constants.Tags.Player).GetComponent<Transform>();
		waterTransform = GameObject.FindGameObjectWithTag(Constants.Tags.Water).transform;
		startOffset = cameraTransform.position - playerTransform.position;
		waterOffset = cameraTransform.position - waterTransform.position;
	}

	void Update()
	{
		moveVector = playerTransform.position + startOffset;
		//X
		moveVector.x = xCameraPosition;
		//Y
		moveVector.y = Mathf.Clamp(moveVector.y,minCameraHeight,maxCameraHeight);
		if(transition > 1.0f)
		{
			cameraTransform.position = moveVector;
		} else 
		{
			 //animation at the game start 
			 cameraTransform.position = Vector3.Lerp(moveVector + animationOffset, moveVector, transition);
			 transition += Time.deltaTime * 1 / startAnimationDuration;
			 cameraTransform.LookAt(playerTransform.position + Vector3.up);
		}
		waterTransform.position = new Vector3(cameraTransform.position.x - waterOffset.x, yWavePosition, cameraTransform.position.z - waterOffset.z);			
	}	
		
}























