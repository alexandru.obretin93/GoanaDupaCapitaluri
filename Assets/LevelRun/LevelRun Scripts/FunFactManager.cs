﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class FunFactManager : MonoBehaviour {

	public float funFactDuration;
	public static FunFactLevel level;
	public FunFactLevel currentLevel;
	private static int funFactToDisplay = 0;
	private PlayerMotor playerMotor;
	private Score playerScore;
	void Start()
	{
		funFactToDisplay = 0;
		playerMotor = GameObject.FindGameObjectWithTag(Constants.Tags.Player).GetComponent<PlayerMotor>();
		playerScore = GameObject.FindGameObjectWithTag(Constants.Tags.Player).GetComponent<Score>();
	}

	public void DisplayFunFact()
	 {
		currentLevel = level;
		var arrayToSelect = Constants.FunFacts[level];
		var info = arrayToSelect.ElementAt(funFactToDisplay);
		funFactToDisplay++;;
		if(playerMotor != null)
		{
			playerMotor.OnNotificationDisplay();
			playerScore.OnNotificationDisplay();
		}
		NotificationManager.CallNotification(info);
	}

	public void DismissFunFact()
	{
		playerMotor.OnNotificationEnd();
		playerScore.OnNotificationEnd();
	}
}
