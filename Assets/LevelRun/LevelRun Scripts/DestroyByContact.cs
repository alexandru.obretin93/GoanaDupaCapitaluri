﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == Constants.Tags.Player)
		{
			other.gameObject.GetComponent<PlayerMotor>().KillPlayer();
		}
	}
}
